import { DataSourceOptions } from "typeorm";
import { User } from "./users/entities/user.entity";
import { Veiculo } from "./users/entities/veiculo.entity";

export const config: DataSourceOptions = {

    // type: process.env.DB_TYPE as any,
    type: 'postgres',
    // host: process.env.PG_HOST,
    host: 'localhost',
    // port: parseInt(process.env.PG_PORT),
    port: 5432,
    // username: process.env.PG_USER,
    username: 'postgres',
    // password: process.env.PG_PASSWORD,
    password: 'postgres',
    // database: process.env.PG_DB,
    database: 'postgres',
    entities: [User, Veiculo],
    // Não usar o synchronize true em PROD pois PODE PERDER DADOS!!!
    synchronize: true

}