import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt'
import { Request } from 'express';
import { Veiculo } from 'src/users/entities/veiculo.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(Veiculo)
        private veiculoRepository: Repository<Veiculo>,
        private userService: UsersService,
        private jwtService: JwtService,
    ) { }

    async validateUser(username: string, pass: string): Promise<any> {

        const user = await this.userService.findOneByEmail(username)

        if (user && user.senha == pass) {
            const { senha, ...result } = user
            return result
        }

        return null

    }

    async login(user: any) {
        const payload = { username: user.email, sub: user.id }

        return { 
            access_token: this.jwtService.sign(payload)
        }
    }

    async salvarVeiculo(file: Express.Multer.File | any, req: Request) {
        const veiculo = new Veiculo()

        veiculo.user = file.user
        veiculo.placa = req.body.placa
        veiculo.marca = req.body.marca
        veiculo.modelo = req.body.modelo
        veiculo.ano = req.body.ano
        veiculo.url = `${req.protocol}://${req.get('host')}/auth/veiculos/${file.file.filename}`

        return await this.veiculoRepository.save(veiculo)
    }

    async getVeiculo(veiculoUrl: any, userId:any) {
        const veiculo = await this.veiculoRepository.find({
            where: [
                { url: veiculoUrl },
                { user: userId },
            ]
        })

        if (!veiculo) {
            throw new NotFoundException(`Veiculo não encontrado`)
        }

        return veiculo
    }

    async getVeiculos(id: any) {
        const veiculos = await this.veiculoRepository.find({where: { user: id }})
        console.log(veiculos)

        return veiculos
    }


}
