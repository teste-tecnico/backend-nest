import {  Controller,  Param, Post, UseGuards, Get, Request, UploadedFile, UseInterceptors, BadRequestException, Res, Req, NotFoundException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard'
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { Response, Request as RequestExpress } from 'express';
import { User } from 'src/decorators/user.decorator';
import { Veiculo } from 'src/users/entities/veiculo.entity';
@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) { }

    @UseGuards(LocalAuthGuard)
    @Post('login')
    async login(@Request() req) {
        return this.authService.login(req.user)
    }

    @UseGuards(JwtAuthGuard)
    @Get('profile')
    getProfile(@Request() req) {
        return req.user
    }

    @UseGuards(JwtAuthGuard)
    @Post('upload-photo')
    @UseInterceptors(FileInterceptor('file', 
    {
        storage: diskStorage({
            destination: './uploads',
            filename: (req, file, callback) => {
                const name: string = file.originalname.split('.')[0]
                const fileExtension: string = file.originalname.split('.')[1]
                const newFileName: string = name.split(' ').join('_') + '_' + Date.now() + '.' + fileExtension

                callback(null, newFileName)

            },
        }),
        fileFilter: (req, file, callback) => {

            if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                return callback(null, false)
            }

            callback(null, true)
        }
    }
    ))
    async uploadPhotoVeiculo(@UploadedFile() file: Express.Multer.File, @Req() req: RequestExpress, @User() user: any) {
        if (!file) {
            throw new BadRequestException('Tipo de arquivo não suportado')
        } else {
            const response = {
                user: user,
                file
            }

            return await this.authService.salvarVeiculo(response, req)
        }

    }

    @UseGuards(JwtAuthGuard)
    @Get('veiculos/:filename')
    async getPicture(@Param('filename') filename, @Res() res: Response, @User() user: any) {
        if (!user) {
            throw new NotFoundException(`Usuário não encontrado`)
        }

        const fileUrl = `http://localhost:3030/auth/veiculos/${filename}`
        console.log(fileUrl)

        await this.authService.getVeiculo(fileUrl, user.id)

        res.sendFile(filename, {
            root: './uploads'
        })
    }

    @UseGuards(JwtAuthGuard)
    @Get('veiculos/getall/:userid')
    async getVeiculos(@Param('userid') userid, @Res() res: Response, @User() user: any) {
        if (!user) {
            throw new NotFoundException(`Usuário não encontrado`)
        }
        console.log(user.id)

        return await this.authService.getVeiculos(user.id)

    }


}
