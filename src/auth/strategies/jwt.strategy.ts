import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from 'passport-jwt';
import { jwtConstants } from "../constants";
import { UsersService } from "src/users/users.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor(private userService: UsersService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false, 
            secretOrKey: jwtConstants.secret
        })
    }

    async validate(payload: any) {

        // aqui podemos fazer buscar no DB e popular o objeto do user com mais informações que podemos eventualmente colocar
        return { id: payload.sub, username: payload.username }

    }

}