import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Veiculo {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => User, user => user.veiculos)
    user: User

    @Column({nullable: false})
    url: string

    @Column({nullable: false})
    placa: string;

    @Column({nullable: false})
    marca: string;

    @Column({nullable: false})
    modelo: string;

    @Column({nullable: false})
    ano: number

}