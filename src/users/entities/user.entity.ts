import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { Veiculo } from "./veiculo.entity";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nome: string;

    @Column()
    email: string;

    @Column({ type: 'bigint' })
    telefone: number;

    @Column()
    senha: string;

    @OneToMany(() => Veiculo, veiculo => veiculo.user) 
    veiculos: Veiculo[]

}
