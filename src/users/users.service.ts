import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const newUser = this.userRepository.create(createUserDto)
    return await this.userRepository.save(newUser)
  }

  async findAll(): Promise<User[]> {
    return await this.userRepository.find()
  }

  async findOne(id: number): Promise<User> {
    return await this.userRepository.findOne({where: { id }})
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const oldUser = await this.userRepository.findOne({ where: { id }})

    if (!oldUser) {
      throw new NotFoundException(`Usuário com a id - ${id} não foi encontrado`)
    }

    await this.userRepository.update(id, updateUserDto)
    return this.userRepository.findOne({ where: { id }})
  }

  async delete(id: number): Promise<void> {
    const user = await this.userRepository.findOne({ where: { id }})

    if (!user) {
      throw new NotFoundException(`Usuário com id - ${id} não foi encontrado`)
    } 

    await this.userRepository.delete(id)
  }

  async findOneByEmail(username: string) {
    return await this.userRepository.findOneBy({ email: username })
  }

}
